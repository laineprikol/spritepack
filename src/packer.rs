use std::cmp;

#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub struct Size {
    pub w: u32,
    pub h: u32,
}

impl Size {
    pub fn new(w: u32, h: u32) -> Size {
        Size { w, h }
    }

    pub fn area(self) -> u32 {
        self.w * self.h
    }

    pub fn perimeter(self) -> u32 {
        2 * (self.w + self.h)
    }

    pub fn bigger_side(self) -> u32 {
        std::cmp::max(self.w, self.h)
    }

    pub fn fits(self, rect: Size) -> bool {
        self.w <= rect.w && self.h <= rect.h
    }

    pub fn pathological_multiplier(self) -> u32 {
        cmp::max(self.w, self.h) / cmp::min(self.w, self.h) * self.w * self.h
    }
}

impl From<(u32, u32)> for Size {
    fn from(v: (u32, u32)) -> Size {
        Size::new(v.0, v.1)
    }
}

impl From<[u32; 2]> for Size {
    fn from(v: [u32; 2]) -> Size {
        Size::new(v[0], v[1])
    }
}

#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub struct Point {
    pub x: u32,
    pub y: u32,
}

impl Point {
    pub fn new(x: u32, y: u32) -> Point {
        Point { x, y }
    }
}

#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub struct Rect {
    pub origin: Point,
    pub size: Size,
}

impl Rect {
    pub fn new(origin: Point, size: Size) -> Rect {
        Rect { origin, size }
    }
}

#[derive(Copy, Clone, Debug, Eq, PartialEq)]
pub enum Sorting {
    Area,
    Perimeter,
    BiggerSide,
    Width,
    Height,
    Pathological,
}

#[derive(Debug)]
pub struct Packer {
    bin_size: Size,
    sorting_order: Vec<Sorting>,
    sorting: Sorting,
    empty_spaces: Vec<Rect>,
}

impl Packer {
    pub fn new(bin_size: Size) -> Packer {
        Packer {
            bin_size,
            empty_spaces: Vec::with_capacity(1024),
            sorting_order: vec![
                Sorting::Area,
                Sorting::Perimeter,
                Sorting::BiggerSide,
                Sorting::Width,
                Sorting::Height,
                Sorting::Pathological,
            ],
            sorting: Sorting::BiggerSide,
        }
    }

    fn reset(&mut self) {
        self.empty_spaces.clear();
        self.empty_spaces
            .push(Rect::new(Point::new(0, 0), self.bin_size));
    }

    fn find_empty_space(&self, rect: Size) -> Option<usize> {
        for (i, &space) in self.empty_spaces.iter().enumerate().rev() {
            if rect.fits(space.size) {
                return Some(i);
            }
        }

        None
    }

    fn split(&mut self, space_i: usize, rect: Size) -> Rect {
        let space = self.empty_spaces[space_i];
        self.empty_spaces.remove(space_i);

        if space.size == rect {
            return space;
        } else if space.size.w == rect.w {
            let split = Rect::new(
                Point::new(space.origin.x, space.origin.y + rect.h),
                Size::new(space.size.w, space.size.h - rect.h),
            );

            self.empty_spaces.push(split);
        } else if space.size.h == rect.h {
            let split = Rect::new(
                Point::new(space.origin.x + rect.w, space.origin.y),
                Size::new(space.size.w - rect.w, space.size.h),
            );

            self.empty_spaces.push(split);
        } else {
            let split_big = Rect::new(
                Point::new(space.origin.x, space.origin.y + rect.h),
                Size::new(space.size.w, space.size.h - rect.h),
            );

            let split_small = Rect::new(
                Point::new(space.origin.x + rect.w, space.origin.y),
                Size::new(space.size.w - rect.w, rect.h),
            );

            self.empty_spaces.push(split_big);
            self.empty_spaces.push(split_small);
        }

        Rect::new(space.origin, rect)
    }

    fn pack_(&mut self, input: &mut [(usize, Size)], output: &mut [Rect]) -> bool {
        match self.sorting {
            Sorting::Area => input.sort_by(|(_, a), (_, b)| b.area().cmp(&a.area())),
            Sorting::Perimeter => input.sort_by(|(_, a), (_, b)| b.perimeter().cmp(&a.perimeter())),
            Sorting::BiggerSide => {
                input.sort_by(|(_, a), (_, b)| b.bigger_side().cmp(&a.bigger_side()))
            }
            Sorting::Width => input.sort_by(|(_, a), (_, b)| b.w.cmp(&a.w)),
            Sorting::Height => input.sort_by(|(_, a), (_, b)| b.h.cmp(&a.h)),
            Sorting::Pathological => input.sort_by(|(_, a), (_, b)| {
                b.pathological_multiplier()
                    .cmp(&a.pathological_multiplier())
            }),
        };

        for &(i, rect) in input.iter() {
            let space = match self.find_empty_space(rect) {
                Some(v) => v,
                None => return false,
            };

            let loc = self.split(space, rect);
            output[i] = loc;
        }

        true
    }

    pub fn pack<I>(&mut self, input: I) -> Option<Vec<Rect>>
    where
        I: IntoIterator,
        I::Item: Into<Size>,
    {
        let mut input = input
            .into_iter()
            .map(|v| v.into())
            .enumerate()
            .collect::<Vec<_>>();

        let mut output = input
            .iter()
            .map(|(_, s)| Rect::new(Point::new(0, 0), *s))
            .collect::<Vec<_>>();

        for sorting in self.sorting_order.clone() {
            self.reset();
            self.sorting = sorting;

            if self.pack_(&mut input, &mut output) {
                return Some(output);
            }
        }

        None
    }
}
